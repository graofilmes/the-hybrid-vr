using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

  class CanvasManager : MonoBehaviour {

    public GameObject bg;

    public CanvasGroup opening;
    public CanvasGroup menu;
    public CanvasGroup about;

    public void StartGame()
    {

        StartCoroutine(FadeCanvasGroup(opening, true, .5f));
        StartCoroutine(FadeCanvasGroup(menu, false, .5f));
    }

    public void StartVideo()
    {
        StartCoroutine(FadeCanvasGroup(menu, true, .5f));
        bg.SetActive(false);
    }

    public void openAbout()
    {
        StartCoroutine(FadeCanvasGroup(opening, true, .5f));
        StartCoroutine(FadeCanvasGroup(about, false, .5f));
    }

     public void closeAbout()
    {
        StartCoroutine(FadeCanvasGroup(about, true, .5f));
        StartCoroutine(FadeCanvasGroup(opening, false, .5f));
    }

    public void EndVideo()
    {
        StartCoroutine(FadeCanvasGroup(menu, false, .5f));
        bg.SetActive(true);
    }

    public IEnumerator FadeCanvasGroup(CanvasGroup cg, bool isHiding, float lerpTime = 1)
	{
		float _timeStartedLerping = Time.time;
		float timeSinceStarted = Time.time - _timeStartedLerping;
		float percentageComplete = timeSinceStarted / lerpTime;

		while (true)
		{
			timeSinceStarted = Time.time - _timeStartedLerping;
			percentageComplete = timeSinceStarted / lerpTime;

			float currentValue = Mathf.Lerp(1, 0, percentageComplete);

            cg.alpha = isHiding ? currentValue : 1 - currentValue;

            if (percentageComplete >= 1) {
                cg.interactable = !cg.interactable;
                cg.blocksRaycasts = !cg.blocksRaycasts;
                break;
            }

			yield return new WaitForFixedUpdate();
		}
	}

}

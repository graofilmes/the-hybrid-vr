using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Video;

public class VideoManager : MonoBehaviour
{

    public VideoEvent onPause = new VideoEvent();
    public VideoEvent onEnd = new VideoEvent();

    public UnityEvent ended;

    public GameObject courtain;

    //private bool isInTransition = false;

    private bool isPaused = false;

    // private float duration = 0.5f;
    public bool IsPaused
    {
        get
        {
            return isPaused;
        }

        private set
        {
            isPaused = value;
            onPause.Invoke(isPaused);
        }
    }

    private bool isReady = false;
    public bool IsReady
    {
        get
        {
            return isReady;
        }

        private set
        {
            isReady = value;
        }
    }

    // private int index = 0;
    private VideoPlayer videoPlayer = null;

    private void Awake()
    {
        videoPlayer = GetComponent<VideoPlayer>();
        videoPlayer.prepareCompleted += OnReady;
        videoPlayer.loopPointReached += OnEnd;
    }

    private void Update()
    {
        OculusInput();
    }

    private void OculusInput()
    {
        if(OVRInput.GetDown(OVRInput.Button.One, OVRInput.Controller.All))
        {
            PauseToggle();
        }

        if(OVRInput.GetDown(OVRInput.Button.Start))
        {
            EndVideo();
        }
    }


    public void PauseToggle()
    {
        if(videoPlayer.isPlaying)
            videoPlayer.Pause();
        else
            videoPlayer.Play();
    }

    public void ChangeVideo(string url)
    {
        isReady = false;
        videoPlayer.source = VideoSource.Url;
        videoPlayer.url = url;
        videoPlayer.Prepare();
    }

    private void OnReady(VideoPlayer videoPlayer)
    {
        isReady = true;
        videoPlayer.Play();
    }

    private void OnEnd(VideoPlayer videoPlayer) {
        EndVideo();
    }

    public void EndVideo() {
        videoPlayer.Stop();
        onEnd.Invoke(true);
        ended.Invoke();
    }

    // private void FadeDomeIn() {
    //     if(!isInTransition)
    //         FadeDome(false);
    // }

    // private void FadeDomeOut() {
    //     if(!isInTransition)
    //         FadeDome(true);
    // }


    // public IEnumerator FadeDome(bool isHiding)
	// {
    //     isInTransition = true;
	// 	float _timeStartedLerping = Time.time;
	// 	float timeSinceStarted = Time.time - _timeStartedLerping;
	// 	float percentageComplete = timeSinceStarted / duration;

	// 	while (true)
	// 	{
	// 		timeSinceStarted = Time.time - _timeStartedLerping;
	// 		percentageComplete = timeSinceStarted / duration;

	// 		float currentValue = Mathf.Lerp(1, 0, percentageComplete);

    //         float alpha = isHiding ? 1 - currentValue : currentValue;

    //         dome.transform.GetComponent<Renderer>().material.color = new Color(1, 1, 1, alpha);

    //         if (percentageComplete >= 1) {
    //             isInTransition = false;
    //             if(isHiding)
    //                 dome.SetActive(false);
    //             break;
    //         }

	// 		yield return new WaitForFixedUpdate();
	// 	}

    //     print("done");
	// }

    public class VideoEvent : UnityEvent<bool> { }
}